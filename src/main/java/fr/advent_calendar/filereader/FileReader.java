package fr.advent_calendar.filereader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FileReader {

    /*
     * Constructeurs.
     */
    private FileReader() {
    }

    /*
     * Méthodes publiques.
     */
    public static List<String> readFile(String path) {

        List<String> lines = new ArrayList<>();

        try {

            lines = Files.readAllLines(Paths.get("src/main/resources", path));
        } catch (IOException e) {

            e.printStackTrace();
        }

        return lines;
    }
}
