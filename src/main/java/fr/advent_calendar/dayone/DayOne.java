package fr.advent_calendar.dayone;

import java.util.List;

public class DayOne {

    public static final String NUMBER_REGEX = "\\D";

    /*
     * Méthodes publiques.
     */
    public void trebuchet(List<String> input) {

        int sum = 0;

        for (String string : input) {

            sum += this.getCalibrationValue(string);
        }

        System.out.println(sum);
    }

    /*
     * Méthodes privées.
     */
    private int getCalibrationValue(String string) {

        String numericString = string.replaceAll(NUMBER_REGEX, "");

        String stringCalibrationValue = String.format("%c%c", numericString.charAt(0), numericString.charAt(numericString.length() - 1));

        return Integer.parseInt(stringCalibrationValue);
    }
}
