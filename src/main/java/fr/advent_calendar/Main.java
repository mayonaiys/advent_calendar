package fr.advent_calendar;

import fr.advent_calendar.dayone.DayOne;
import fr.advent_calendar.filereader.FileReader;

public class Main {

    /*
     * Méthodes publiques.
     */

    public static void main(String[] args) {

        // Jour 1 - Trebuchet
        trebuchet();

    }

    /*
     * Méthodes privées.
     */

    private static void trebuchet() {

        String filePath = "dayone/dayone_input.txt";

        DayOne dayOne = new DayOne();

        dayOne.trebuchet(FileReader.readFile(filePath));
    }
}